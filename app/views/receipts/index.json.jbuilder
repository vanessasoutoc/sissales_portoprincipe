json.array!(@receipts) do |receipt|
  json.extract! receipt, :id, :order_id, :date, :value
  json.url receipt_url(receipt, format: :json)
end
