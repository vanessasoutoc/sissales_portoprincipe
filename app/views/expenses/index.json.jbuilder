json.array!(@expenses) do |expense|
  json.extract! expense, :id, :account, :maturity_date, :value, :date_payment
  json.url expense_url(expense, format: :json)
end
