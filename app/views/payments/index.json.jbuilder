json.array!(@payments) do |payment|
  json.extract! payment, :id, :form, :date_payment, :date_one_portion, :date_two_portion, :date_three_portion, :date_one_payment, :date_two_payment, :date_three_payment
  json.url payment_url(payment, format: :json)
end
