class Product < ActiveRecord::Base
    has_many :line_items, dependent: :destroy
    has_many :orders, through: :line_items


    # Validations
    validates :name, presence: {message: "Vocẽ precisa prencher esse campo!"}, length: { maximum: 30 }
    validates :description, length: { maximum: 99 }
    validates :code, presence: {message: "Vocẽ precisa prencher esse campo!"},
    uniqueness: {message: "Código já cadastrado!" }

    validates :unit_price,	presence: {message: "Vocẽ precisa prencher esse campo!"},
    numericality: { greater_than_or_equal_to: 0 },
    format: { with: /\A\d+(\.\d{1,2})?\z/i }
    validates :inventory_qty,	presence: {message: "Vocẽ precisa prencher esse campo!"},
    numericality: { greater_than_or_equal_to: 0 }
end
