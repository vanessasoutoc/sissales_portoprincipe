class Order < ActiveRecord::Base
	belongs_to :customer
	has_one :payment
	has_one :entrada
	accepts_nested_attributes_for :payment, allow_destroy: true
	accepts_nested_attributes_for :entrada, allow_destroy: true
	has_many :line_items, dependent: :destroy
	has_many :products, through: :line_items
	has_many :receipt, dependent: :destroy

	# Validations
	validates :description, presence: true, length: { maximum: 50 }
	validates :date, presence: true
	# Since the total is now calculated, this validation is no longer necessary
	#validates :total, presence: true,
	#									numericality: { greater_than_or_equal_to: 0 },
	#									format: { with: /\A\d+(\.\d{1,2})?\z/i }
	# TODO: Format below should handle commas, but doesn't.  Research.
	# Currency amount (cents optional) Optional thousands separators; optional two-digit fraction
	#format: { with: /\A[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?\z/i }

	#def value
	#	sum = 0
	#	line_items.each do |line_item|
	#		sum += line_item.value
	#	end
	#	return sum
	#end

	#busca simples data unica
	#def self.search(query)
	#	where("date >= '#{query}'")
	#end  date_start, date_end

	def valuerecebido
		sub = 0
		if value_receipt != nil && value != nil
			return sub = value - (value_receipt + entrada.value)
		end
	end


	def parcela_2
		sum = 0
		if valuerecebido != nil
			sum = valuerecebido / 2
		end
		return sum
	end

	def parcela_3
		sum = 0
		if valuerecebido != nil
			sum = valuerecebido / 3
		end
		return sum
	end


	def value_receipt
		sum = 0
		receipt.each do |receipt|
			sum += receipt.value
		end
		return sum
	end

	def desconto_entrada
		if entrada.value != nil || entrada.value != 0
			return desconto_entrada = value - entrada.value
		else
			entrada.value = 0
		end
	end

	#busca pelo nome do cliente
	def self.search(client, date_start, date_end)
		joins(:customer).where("LOWER(customers.name) LIKE ? AND date BETWEEN ? AND ?", "%#{client}%", date_start, date_end)
	end

	def self.search_primeira_parcela(de_primeira_parcela, primeira_parcela)
		# joins(:payment).where(("date_one_portion LIKE ?  OR date_two_portion LIKE ? OR date_three_portion OR LIKE ?"), "#{datequery1}%", "#{datequery2}%", "#{datequery}%")
		joins(:payment).where("date_one_payment IS NULL AND date_one_portion BETWEEN ? AND ?", de_primeira_parcela, primeira_parcela)
	end

	def self.search_segunda_parcela(de_segunda_parcela, segunda_parcela)
		# joins(:payment).where(("date_one_portion LIKE ?  OR date_two_portion LIKE ? OR date_three_portion OR LIKE ?"), "#{datequery1}%", "#{datequery2}%", "#{datequery}%")

		joins(:payment).where("date_two_payment IS NULL AND date_two_portion BETWEEN ? AND ?", de_segunda_parcela, segunda_parcela)

	end
	def self.search_terceira_parcela(de_terceira_parcela, terceira_parcela)
		# joins(:payment).where(("date_one_portion LIKE ?  OR date_two_portion LIKE ? OR date_three_portion OR LIKE ?"), "#{datequery1}%", "#{datequery2}%", "#{datequery}%")

		joins(:payment).where("date_three_payment IS NULL AND date_three_portion BETWEEN ? AND ?", de_terceira_parcela, terceira_parcela)

	end


end
