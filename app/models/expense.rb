class Expense < ActiveRecord::Base
    validates :account, presence:  {message: "Você precisa preencher esse campo."}
    validates :maturity_date, presence:  {message: "Você precisa preencher esse campo."}
    validates :value, presence:  {message: "Você precisa preencher esse campo."}
    
    
    
    def self.search(account, date_start, date_end)
       where("LOWER(account) LIKE ? AND maturity_date >= ? AND maturity_date <= ?", "%#{account}%", date_start, date_end)
   end
end
