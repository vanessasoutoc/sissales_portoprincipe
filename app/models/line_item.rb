class LineItem < ActiveRecord::Base
    belongs_to :order
    belongs_to :product



    self.primary_key = :order_id, :product_id

    # Validations
    validates :product_id, presence: true
    validates :quantity,	presence: true,
    numericality: { only_integer: true,
        greater_than_or_equal_to: 1,
        less_than_or_equal_to: 1000000 }
    validates :discount_percent,
    numericality: { only_integer: true,
        greater_than_or_equal_to: 0,
        less_than_or_equal_to: 100 }


    def value
        product.unit_price * quantity * (1 - (discount_percent / 100.0))
    end



    #before_destroy :update_unitproduct
    #before_update :update_unitproduct
    before_destroy :update_unitproduct
    after_update :update_unitproduct
    def update_unitproduct

        product.inventory_qty += saidaest(product)
        product.save!

    end

    # atualiza a quantidade de produtos disponivel para venda
    after_create :update_product_qty
    #after_destroy :update_product_qty
    #after_update :update_product_qty
    def update_product_qty
        product.inventory_qty -= saidaest(product)
        product.save!
    end

    after_create :update_oder_value
    after_destroy :update_oder_value
    after_update :update_oder_value
    def update_oder_value
        @order = Order.find(self.order_id)
        @order.value = total(@order)
        @order.save!
    end

    def total(order)
        sum = 0
        order.line_items.each do |line_item|
            sum += line_item.value
        end
        return sum 
    end


    def saidaest(product)
        saidaestoque = 0
        order.line_items.each do |line_item|
            saidaestoque = line_item.quantity
        end
        return saidaestoque
    end

end