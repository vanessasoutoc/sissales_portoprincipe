$( "#product_id" ).autocomplete({ source: "/controller_name?filter_key=attr_name", minLength: 0 });

$(document).ready(function() {
    //console.log("Document is ready");
    $("#datepicker").datepicker({ dateFormat: "d/m/yy",
                                 dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                 dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                 dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                 monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                 monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                 nextText: 'Próximo',
                                 prevText: 'Anterior' });

    $("#datepicker1").datepicker({ dateFormat: "d/m/yy", dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                  dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                  dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                  monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                  monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                  nextText: 'Próximo',
                                  prevText: 'Anterior' });
	$("#datepicker2").datepicker({ dateFormat: "d/m/yy", dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                  dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                  dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                  monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                  monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                  nextText: 'Próximo',
                                  prevText: 'Anterior' });
	$("#datepicker3").datepicker({ dateFormat: "d/m/yy", dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                  dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                  dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                  monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                  monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                  nextText: 'Próximo',
                                  prevText: 'Anterior' });
	$("#datepicker4").datepicker({ dateFormat: "d/m/yy", dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                  dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                  dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                  monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                  monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                  nextText: 'Próximo',
                                  prevText: 'Anterior' });
	$("#datepicker5").datepicker({ dateFormat: "d/m/yy", dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                  dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                  dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                  monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                  monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                  nextText: 'Próximo',
                                  prevText: 'Anterior' });

    $("#datepickerNewDespesa").datepicker({ dateFormat: "d/m/yy",		
                                           dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                           dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                           dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                           monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                           monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                           nextText: 'Próximo',
                                           prevText: 'Anterior'});

    $("#datepickerVencimentoDespesa").datepicker({ dateFormat: "d/m/yy",
                                                  dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                                  dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                                  dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                                  monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                                  monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                                  nextText: 'Próximo',
                                                  prevText: 'Anterior'});

    $("#datepickerOnePortion").datepicker({dateFormat: "d/m/yy",	
                                           dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                           dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                           dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                           monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                           monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                           nextText: 'Próximo',
                                           prevText: 'Anterior'});

    $("#datepickerOnePayment").datepicker({dateFormat: "d/m/yy",	
                                           dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                           dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                           dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                           monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                           monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                           nextText: 'Próximo',
                                           prevText: 'Anterior'});

    $("#datepickerTwoPortion").datepicker({dateFormat: "d/m/yy",	
                                           dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                           dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                           dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                           monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                           monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                           nextText: 'Próximo',
                                           prevText: 'Anterior'});
    $("#datepickerTwoPayment").datepicker({dateFormat: "d/m/yy",	
                                           dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                           dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                           dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                           monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                           monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                           nextText: 'Próximo',
                                           prevText: 'Anterior'});

    $("#datepickerThreePortion").datepicker({dateFormat: "d/m/yy",	
                                             dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                             dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                             dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                             monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                             monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                             nextText: 'Próximo',
                                             prevText: 'Anterior'});
    $("#datepickerThreePayment").datepicker({dateFormat: "d/m/yy",	
                                             dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                             dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                             dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                             monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                             monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                             nextText: 'Próximo',
                                             prevText: 'Anterior'});



    $("#customer-index-table").tablesorter({
        theme: 'bootstrap',
        initWidgets: true,
        widgets: ['uitheme'],
        widgetOptions: { uitheme: 'bootstrap' },
        headerTemplate: '{content}{icon}',
        sortList: [[0,0]],	// Default ascending sort on 'NAME' column
        headers: {	// Disable sort on 'Detail', 'Edit' and 'Delete' columns
            2: { sorter: false },
            3: { sorter: false },
            4: { sorter: false }
        }
    })
        .tablesorterPager({
        container: $("#customer-pager")}
                         );

    $("#user-index-table").tablesorter({
        theme: 'bootstrap',
        initWidgets: true,
        widgets: ['uitheme'],
        widgetOptions: { uitheme: 'bootstrap' },
        headerTemplate: '{content}{icon}',
        sortList: [[0,0]],	// Default ascending sort on 'NAME' column
        headers: {	// Disable sort on 'Show', 'Edit' and 'Delete' columns
            2: { sorter: false },
            3: { sorter: false },
            4: { sorter: false }
        }
    });

    $("#order-index-table").tablesorter({
        theme: 'bootstrap',
        initWidgets: true,
        widgets: ['uitheme'],
        widgetOptions: { uitheme: 'bootstrap' },
        headerTemplate: '{content}{icon}',
        sortList: [[0,0]],	// Default ascending sort on 'NAME' column
        headers: {	// Disable sort on 'Show', 'Edit' and 'Delete' columns
            2: { sorter: false },
            3: { sorter: false },
            4: { sorter: false }
        }
    });

    $("#product-index-table")
        .tablesorter({
        theme: 'bootstrap',
        initWidgets: true,
        widgets: ['uitheme'],
        widgetOptions: { uitheme: 'bootstrap' },
        headerTemplate: '{content}{icon}',
        sortList: [[0,0]],	// Default ascending sort on 'NAME' column
        headers: {	// Disable sort on 'Show', 'Edit' and 'Delete' columns
            1: { sorter: false },
            4: { sorter: false },
            5: { sorter: false },
            6: { sorter: false },
            7: { sorter: false }
        }
    })
        .tablesorterPager({
        container: $("#product-pager")}
                         );

    $("#lineitem-index-table").tablesorter({
        theme: 'bootstrap',
        initWidgets: true,
        widgets: ['uitheme'],
        widgetOptions: { uitheme: 'bootstrap' },
        headerTemplate: '{content}{icon}',
        sortList: [[0,0]],	// Default ascending sort on 'NAME' column
        headers: {	// Disable sort on 'Edit' and 'Delete' columns
            6: { sorter: false },
            7: { sorter: false }
        }
    });


    $('.print').click(function(){
        $('.contentPrint').printArea({
            mode:'iframe',
            popHt: 500,
            popWt: 400,
            popX: 1000,
            popY: 600,
            //popTitle: 'Título da janela'
        });
        //return false;
    });



});
