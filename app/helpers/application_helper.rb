module ApplicationHelper
    # Product view table sorting implementation
    def sortable(column, title = nil)
        title ||= column.codease
        link_class = column == sort_column ? "current #{sort_direction}" : nil
        direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"

        span_class = ""
        if (column != sort_column)
            span_class = "glyphicon glyphicon-sort"
        elsif (direction == "asc")
            span_class = "glyphicon glyphicon-arrow-down"
        elsif (direction == "desc")
            span_class = "glyphicon glyphicon-arrow-up"
        end

        link_to({sort: column, direction: direction}, {class: link_class}) do
            (title + ' ' + content_tag(:span, "", class: span_class)).html_safe
        end
    end

    def type_payment
        [
            ['A vista', 'A vista'],
            ['1x Parcela', '1x Parcela'],
            ['2x Parcelas', '2x Parcelas'],
            ['3x Parcelas', '3x Parcelas']
            ]
    end

    def type_busca
        [
            ['vencimento', 'Vencimento'],
            ['1 Parcela', '1 Parcela'],
            ['2 Parcela', '2 Parcelas'],
            ['3 Parcela', '3 Parcelas']
            ]
    end

    def type_order
        [['VENDA', 'VENDA']]
    end

    # Used to populate state select control in Customer add and edit form
    def us_states
        [
            ['Acre', 'Acre'],
            ['Alagoas', 'Alagoas'],
            ['Amazonas', 'Amazonas'],
            ['Amapa', 'Amapa'],
            ['Bahia', 'Bahia'],
            ['Ceará', 'Ceará'],
            ['Distrito Federal', 'Distrito Federal'],
            ['Espirito Santo', 'Espirito Santo'],
            ['Goiás', 'Goiás'],
            ['Maranhão', 'Maranhão'],
            ['Mato Grosso', 'Mato Grosso'],
            ['Minas Gerais', 'Minas Gerais'],
            ['Pará', 'Pará'],
            ['Paraíba', 'Paraíba'],
            ['Paraná', 'Paraná'],
            ['Pernambuco', 'Pernambuco'],
            ['Piauí', 'Piauí'],
            ['Rio de Janeiro', 'Rio de Janeiro'],
            ['Rio Grande do Norte', 'Rio Grande do Norte'],
            ['Rio Grande do Sul', 'Rio Grande do Sul'],
            ['Rondônia', 'Rondônia'],
            ['Roraíma', 'Roraíma'],
            ['Santa Catarina', 'Santa Catarina'],
            ['São Paulo', 'São Paulo'],
            ['Sergipe', 'Sergipe'],
            ['Tocantins', 'Tocantis']

            ]
    end

end
