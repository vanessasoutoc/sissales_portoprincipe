class OrdersController < ApplicationController
	before_action :find_order, only: [:show, :edit, :update, :destroy]
	before_action :find_customer, only: [:new, :create]

	def payment
		@payment
	end

	def entrada
		@entrada
	end
	def receipt
		@receipt
	end

	#def paymentes_atributes=(attributes)
	# Process the attributes hash
	#end

	def index
		client = params[:client]
		date_start = params[:date_start]
		date_end = params[:date_end]
		if client != "" || date_start != "" && date_end != ""

			if date_start != nil && date_end != nil
				if date_end > date_start

					date_start = Date.parse(date_start)
					date_end = Date.parse(date_end)
					date_end = date_end + 1.days
				else
					flash[:danger] = "A data final tem que ser maior que data inicial";
				end


			end
			@orders = Order.search(client, date_start, date_end).order("date ASC")

		else
			flash[:success] = "Você realizou uma busca completa!";
			@orders = Order.all
		end
	end

	def info
		#client = params[:client]

		#datequery1 = params[:date_vencimento]
		#datequery1 = params[:date_vencimento]
		de_primeira_parcela =  params[:de_primeira_parcela]
		de_segunda_parcela = params[:de_segunda_parcela]
		de_terceira_parcela = params[:de_terceira_parcela]
		primeira_parcela = params[:primeira_parcela]
		segunda_parcela = params[:segunda_parcela]
		terceira_parcela = params[:terceira_parcela]
		#datequery2 = datequery1
		#datequery0 = params[:datequery0]


		if primeira_parcela != ""
			if primeira_parcela == nil
				flash[:warning] = "Prencha a data desejada"
				#@orders = Order.search_venc(datequery1).order("date ASC")
			else
				primeira_parcela = Date.parse(primeira_parcela) +1.days
				de_primeira_parcela = Date.parse(de_primeira_parcela)
				#datequery0 = ''
				#flash[:danger] = "Você realizou " + datequery1.strftime('%-d/%-m/%-Y')
				#@orders = Order.search_venc(datequery1).order("date ASC")
				flash[:success] = "Você realizou uma busca em data da 1ª parcela com vencimento de #{de_primeira_parcela.strftime("%-d/%-m/%Y")} a #{primeira_parcela.strftime("%-d/%-m/%Y")}.";
	end
	@orders = Order.search_primeira_parcela(de_primeira_parcela, primeira_parcela).order("date ASC");

else if segunda_parcela != ""
	if segunda_parcela == nil
		flash[:warning] = "Prencha a data desejada"
		#@orders = Order.search_venc(datequery1).order("date ASC")
	else
		segunda_parcela = Date.parse(segunda_parcela) +1.days
		de_segunda_parcela = Date.parse(de_segunda_parcela)
		#datequery0 = ''
		#flash[:danger] = "Você realizou " + datequery1.strftime('%-d/%-m/%-Y')
		#@orders = Order.search_venc(datequery1).order("date ASC")
		flash[:success] = "Você realizou uma busca em data da 2ª parcela com vencimento de #{de_segunda_parcela.strftime("%-d/%-m/%Y")} à #{segunda_parcela.strftime("%-d/%-m/%Y")}.";

end
@orders = Order.search_segunda_parcela(de_segunda_parcela, segunda_parcela).order("date ASC");
#flash[:success] = "Você realizou uma busca completa!";

else if terceira_parcela != ""
if terceira_parcela == nil
	flash[:warning] = "Prencha a data desejada"
	#@orders = Order.search_venc(datequery1).order("date ASC")
else
	terceira_parcela = Date.parse(terceira_parcela) +1.days
	de_terceira_parcela = Date.parse(de_terceira_parcela)
	#datequery0 = ''
	#flash[:danger] = "Você realizou " + datequery1.strftime('%-d/%-m/%-Y')
	#@orders = Order.search_venc(datequery1).order("date ASC")
	flash[:success] = "Você realizou uma busca em data da 3ª parcela com vencimento de #{de_terceira_parcela.strftime("%-d/%-m/%Y")} à #{terceira_parcela.strftime("%-d/%-m/%Y")}.";
end
@orders = Order.search_terceira_parcela(de_terceira_parcela, terceira_parcela).order("date ASC");
#flash[:success] = "Você realizou uma busca completa!";
end

end
end
end

def show

end


def new
	@order = Order.new
	@order.build_payment
	@order.build_entrada

end

def create
	@order = @customer.orders.build(order_params)
	if @order.save

		flash[:success] = "Ordem adicionada com sucesso!"
		redirect_to @order
	else
		render "new"
	end
end

def edit
end


def update
	if @order.update_attributes(order_params)
		flash[:success] = "Ordem alterada com sucesso!"
		redirect_to @order
	else
		render "edit"
	end
end

def destroy
	@customer = Customer.find(@order.customer_id)
	@order.destroy
	flash[:success] = "Ordem excluída com sucesso!"
	redirect_to customer_path(@customer)
end



private
def order_params
	# This method is called before the validations, so verify date is not blank for new Order
	#unless params[:order][:date].blank?
	#	params[:order][:date] = DateTime.strptime(params[:order][:date], "%d/%m/%Y").strftime("%Y-%m-%d")
	#end
	params.require(:order).permit(:description, :date, :order, :value, payment_attributes: [:form, :date_payment, :date_one_portion, :date_two_portion, :date_three_portion, :date_one_payment, :date_two_payment, :date_three_payment], entrada_attributes: [:value], receipt_attibutes: [:date, :value])
end

def find_customer
	@customer = Customer.find(params[:customer_id])
end

def find_order
	@order = Order.find(params[:id])
end
end
