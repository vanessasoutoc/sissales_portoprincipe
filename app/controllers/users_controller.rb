﻿class UsersController < ApplicationController
	before_action :admin_user
	def index
		@users = User.all
	end

	def show
		@user = User.find(params[:id])
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			flash[:success] = "Usuário '#{@user.name}' adicionado com sucesso!"
			redirect_to @user
		else
			render "new"
		end
	end

	def edit
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])
		if @user.update(name: params[:user][:name])
			flash[:success] = "Usuário alterado com sucesso!"
			redirect_to @user
		else
			render "edit"
		end
	end

	def destroy
		if current_user.id == params[:id].to_i
			flash[:danger] = "Você não pode excluir um usuário logado."
			redirect_to users_path
		else
			User.find(params[:id]).destroy
			flash[:success] = "Usuário excluído com sucesso."
			redirect_to users_path
		end
	end

	# comente if end unless end
	private
	def user_params
		if current_user && current_user.admin?
			params.require(:user).permit(:name, :email, :password, :password_confirmation, :admin)
		end
	end

	def admin_user
		unless current_user.admin?
			flash[:warning] = "You must be an admin to access the requested option."
			redirect_to root_url
		end
	end
end
