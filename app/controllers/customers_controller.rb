class CustomersController < ApplicationController
	before_action :find_customer, only: [:show, :edit, :update]

	def index
		if params[:search]
            @customers = Customer.search(params[:search]).order("name ASC")
        else
            @customers = Customer.all.order('name ASC')
        end
	end

	def show
	end

	def new
		@customer = Customer.new
	end

	def create
		@customer = Customer.new(customer_params)
		if @customer.save
			flash[:success] = "Cliente adicionado com sucesso!"
			redirect_to @customer
		else
			render "new"
		end
	end

	def edit
	end

	def update
		if @customer.update_attributes(customer_params)
			flash[:success] = "Cliente alterado com sucesso!"
			redirect_to @customer
		else
			render "edit"
		end
	end

	def destroy
		Customer.find(params[:id]).destroy
		flash[:success] = "Cliente excluído com sucesso!"
		redirect_to customers_path
	end
	
	private
		def customer_params
			params.require(:customer).permit(:name, :street, :city, :state, :zipcode, :homephone, :cellphone, :email, :cpf, :name_mother, :rg)
		end

		def find_customer
			@customer = Customer.find(params[:id])
		end
end
