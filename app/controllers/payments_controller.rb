class PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  # GET /payments
  # GET /payments.json
  def index
    @payments = Payment.all
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  # GET /payments/new
  def new
    @payment = Payment.new
  end

  # GET /payments/1/edit
  def edit
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new(payment_params)
      if @payment.save
        flash[:success] = "Pagamento adicionado com sucesso!"
          redirect_to @payment
      else
          flash[:danger] = "Despesa não foi adicionado!"
        render "new"
      end
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
      if @payment.update(payment_params)
        flash[:success] = "Pagamento alterado com sucesso!"
          redirect_to @payment
      else
        flash[:danger] = "Pagamento não foi alterado!"
          render "edit"
      end

  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:form, :date_payment, :date_one_portion, :date_two_portion, :date_three_portion, :date_one_payment, :date_two_payment, :date_three_payment)
    end
end
