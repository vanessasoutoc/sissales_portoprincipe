class ExpensesController < ApplicationController
    before_action :set_expense, only: [:show, :edit, :update, :destroy]

    # GET /expenses
    # GET /expenses.json
    def index
        account = params[:account]
		date_start = params[:date_start] 
		date_end = params[:date_end]
		if account != "" || date_start != "" && date_end != ""
			@expenses = Expense.search(account, date_start, date_end).order("maturity_date ASC")
		else
			flash[:success] = "Você realizou uma busca completa!";
			@expenses = Expense.all
		end

    end

    # GET /expenses/1
    # GET /expenses/1.json
    def show


    end

    # GET /expenses/new
    def new
        @expense = Expense.new
    end

    # GET /expenses/1/edit
    def edit

    end

    # POST /expenses
    # POST /expenses.json
    def create
        @expense = Expense.new(expense_params)
        if @expense.save
            flash[:success] = "Despesa adicionada com sucesso!"
            redirect_to @expense
        else
            render "new"

        end
    end

    # PATCH/PUT /expenses/1
    # PATCH/PUT /expenses/1.json
    def update

        if @expense.update(expense_params)
            flash[:success] = "Despesa alterada com sucesso!"
            redirect_to @expense
        else
            render "edit"

        end
    end

    # DELETE /expenses/1
    # DELETE /expenses/1.json
    def destroy
        if @expense.destroy
            flash[:success] = "Despesa excluída com sucesso!"
            redirect_to @expense
        else
            flash[:danger] = "Despesa não pode ser excluída!"
            redirect_to @expense
        end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_expense
        @expense = Expense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def expense_params
        params.require(:expense).permit(:account, :maturity_date, :value, :date_payment)
    end
end
