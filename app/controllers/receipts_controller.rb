class ReceiptsController < ApplicationController
    before_action :set_receipt, only: [:show, :edit, :update, :destroy]
    before_action :find_order, only: [:new, :create]
    
    # GET /receipts
    # GET /receipts.json
    def index
        @receipts = Receipt.all
    end

    # GET /receipts/1
    # GET /receipts/1.json
    def show
    end

    # GET /receipts/new
    def new
        @receipt = Receipt.new
    end

    # GET /receipts/1/edit
    def edit
    end

    # POST /receipts
    # POST /receipts.json
    def create
        @receipt = @order.receipt.build(receipt_params)
        respond_to do |format|
            if @receipt.save
                format.html { redirect_to @order, notice: 'Receipt was successfully created.' }
                format.json { render action: 'show', status: :created, location: @receipt }
            else
                format.html { render action: 'new' }
                format.json { render json: @receipt.errors, status: :unprocessable_entity }
            end
        end
    end

    # PATCH/PUT /receipts/1
    # PATCH/PUT /receipts/1.json
    def update
        respond_to do |format|
            if @receipt.update(receipt_params)
                format.html { redirect_to @order, notice: 'Receipt was successfully updated.' }
                format.json { head :no_content }
            else
                format.html { render action: 'edit' }
                format.json { render json: @receipt.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /receipts/1
    # DELETE /receipts/1.json
    def destroy
        @order = Order.find(@receipt.order_id)
        @receipt.destroy
        respond_to do |format|
            format.html { redirect_to order_path(@order) }
            format.json { head :no_content }
        end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_receipt
        @receipt = Receipt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def receipt_params
        params.require(:receipt).permit(:date, :value)
    end
    def find_order
        @order = Order.find(params[:order_id])
    end
    def find_receipt
        @receipt = Receipt.find(params[:id])
    end

end
