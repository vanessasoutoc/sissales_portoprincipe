Associations::Application.routes.draw do
    resources :receipts

    resources :entradas

    resources :payments

    resources :expenses

    root  'customers#index'

    resources :orders do
        resources :receipts, shallow: true
    end

    resources :line_items do
        get :autocomplete_product_code, :on => :collection
    end

    resources :customers do
        resources :orders, shallow: true
    end
    resources :customers, shallow: true do
        resources :orders do
            resources :line_items, only: [:new, :create, :edit, :update, :destroy, :autocomplete]
        end
    end



    resources :users
    resources :products, controllers: {line_item: 'line_item'}
    resources :sessions, only: [:new, :create, :destroy]
    resources :orders

    get '/signin' => 'sessions#new'
    delete '/signout' => 'sessions#destroy'

    get 'info' => "orders#info", :as => :order_info
    #get 'search' => "pedidos#search", :as => :pedidos_search
    #get '/contact', to: 'static_pages#show', id: 'contact'

    # NOTE: This block began causing an error after I added code to the 'SessionsHelper' module
    # and included it in 'ApplicationController'

    #StaticPagesController.action_methods.each do |action|
    #  get "/#{action}", to: "static_pages##{action}", as: "#{action}_static_page"
    #end

    # The priority is based upon order of creation: first created -> highest priority.
    # See how all your routes lay out with "rake routes".

    # You can have the root of your site routed with "root"
    # root 'welcome#index'

    # Example of regular route:
    #   get 'products/:id' => 'catalog#view'

    # Example of named route that can be invoked with purchase_url(id: product.id)
    #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

    # Example resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Example resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end

    # Example resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Example resource route with more complex sub-resources:
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', on: :collection
    #     end
    #   end

    # Example resource route with concerns:
    #   concern :toggleable do
    #     post 'toggle'
    #   end
    #   resources :posts, concerns: :toggleable
    #   resources :photos, concerns: :toggleable

    # Example resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end
end
