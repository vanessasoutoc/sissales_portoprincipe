class AddOrderIdToEntradas < ActiveRecord::Migration
  def change
    add_reference :entradas, :order, index: true
  end
end
