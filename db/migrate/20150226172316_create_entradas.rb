class CreateEntradas < ActiveRecord::Migration
  def change
    create_table :entradas do |t|
      t.decimal :value

      t.timestamps
    end
  end
end
