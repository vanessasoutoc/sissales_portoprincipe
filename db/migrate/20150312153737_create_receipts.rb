class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.references :order, index: true
      t.date :date
      t.decimal :value

      t.timestamps
    end
  end
end
