class AddValueToLineItem < ActiveRecord::Migration
  def change
    add_column :line_items, :value, :decimal
  end
end
