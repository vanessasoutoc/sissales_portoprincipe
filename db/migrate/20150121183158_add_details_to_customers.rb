class AddDetailsToCustomers < ActiveRecord::Migration
  def change
  	add_column :customers, :street, :string
    add_column :customers, :city, :string
    add_column :customers, :state, :string
    add_column :customers, :zipcode, :string
    add_column :customers, :homephone, :string
    add_column :customers, :cellphone, :string
    add_column :customers, :email, :string
  	add_column :customers, :cpf, :string, limit: 20
    add_column :customers, :name_mother, :string, limit: 50
    add_column :customers, :rg, :string, limit: 20
  end
end
