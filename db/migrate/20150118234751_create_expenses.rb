class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :account
      t.datetime :maturity_date
      t.decimal :value
      t.datetime :date_payment

      t.timestamps
    end
  end
end
