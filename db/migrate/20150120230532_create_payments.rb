class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :form
      t.datetime :date_payment
      t.datetime :date_one_portion
      t.datetime :date_two_portion
      t.datetime :date_three_portion
      t.datetime :date_one_payment
      t.datetime :date_two_payment
      t.datetime :date_three_payment

      t.timestamps
    end
  end
end
