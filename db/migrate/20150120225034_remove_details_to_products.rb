class RemoveDetailsToProducts < ActiveRecord::Migration
  def change
    remove_column :products, :upc, :decimal
  end
end
