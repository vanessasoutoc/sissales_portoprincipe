# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150312153737) do

  create_table "customers", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "zipcode"
    t.string   "homephone"
    t.string   "cellphone"
    t.string   "email"
    t.string   "cpf",         limit: 20
    t.string   "name_mother", limit: 50
    t.string   "rg",          limit: 20
  end

  create_table "entradas", force: true do |t|
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
  end

  add_index "entradas", ["order_id"], name: "index_entradas_on_order_id"

  create_table "expenses", force: true do |t|
    t.string   "account"
    t.datetime "maturity_date"
    t.decimal  "value"
    t.datetime "date_payment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "line_items", id: false, force: true do |t|
    t.integer  "order_id",                     null: false
    t.integer  "product_id",                   null: false
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "discount_percent", default: 0
    t.decimal  "value"
  end

  create_table "orders", force: true do |t|
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "customer_id"
    t.string   "description"
    t.decimal  "value"
  end

  add_index "orders", ["customer_id"], name: "index_orders_on_customer_id"

  create_table "payments", force: true do |t|
    t.string   "form"
    t.datetime "date_payment"
    t.datetime "date_one_portion"
    t.datetime "date_two_portion"
    t.datetime "date_three_portion"
    t.datetime "date_one_payment"
    t.datetime "date_two_payment"
    t.datetime "date_three_payment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
  end

  add_index "payments", ["order_id"], name: "index_payments_on_order_id"

  create_table "products", force: true do |t|
    t.string   "name",          limit: 30
    t.string   "description"
    t.decimal  "unit_price",               precision: 10, scale: 2
    t.integer  "inventory_qty"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code",          limit: 50
  end

  add_index "products", ["name"], name: "index_products_on_name"

  create_table "receipts", force: true do |t|
    t.integer  "order_id"
    t.date     "date"
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "receipts", ["order_id"], name: "index_receipts_on_order_id"

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.string   "persistent_session_token"
    t.boolean  "admin",                    default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["persistent_session_token"], name: "index_users_on_persistent_session_token"

end
