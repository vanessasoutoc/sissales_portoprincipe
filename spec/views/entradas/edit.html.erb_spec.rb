require 'spec_helper'

describe "entradas/edit" do
  before(:each) do
    @entrada = assign(:entrada, stub_model(Entrada,
      :value => "9.99"
    ))
  end

  it "renders the edit entrada form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", entrada_path(@entrada), "post" do
      assert_select "input#entrada_value[name=?]", "entrada[value]"
    end
  end
end
