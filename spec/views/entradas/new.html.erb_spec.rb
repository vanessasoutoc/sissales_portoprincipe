require 'spec_helper'

describe "entradas/new" do
  before(:each) do
    assign(:entrada, stub_model(Entrada,
      :value => "9.99"
    ).as_new_record)
  end

  it "renders new entrada form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", entradas_path, "post" do
      assert_select "input#entrada_value[name=?]", "entrada[value]"
    end
  end
end
