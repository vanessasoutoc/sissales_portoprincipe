require 'spec_helper'

describe "entradas/index" do
  before(:each) do
    assign(:entradas, [
      stub_model(Entrada,
        :value => "9.99"
      ),
      stub_model(Entrada,
        :value => "9.99"
      )
    ])
  end

  it "renders a list of entradas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
