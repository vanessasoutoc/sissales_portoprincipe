require 'spec_helper'

describe "expenses/new" do
  before(:each) do
    assign(:expense, stub_model(Expense,
      :account => "MyString",
      :value => "9.99"
    ).as_new_record)
  end

  it "renders new expense form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", expenses_path, "post" do
      assert_select "input#expense_account[name=?]", "expense[account]"
      assert_select "input#expense_value[name=?]", "expense[value]"
    end
  end
end
