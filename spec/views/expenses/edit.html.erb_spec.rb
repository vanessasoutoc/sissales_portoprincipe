require 'spec_helper'

describe "expenses/edit" do
  before(:each) do
    @expense = assign(:expense, stub_model(Expense,
      :account => "MyString",
      :value => "9.99"
    ))
  end

  it "renders the edit expense form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", expense_path(@expense), "post" do
      assert_select "input#expense_account[name=?]", "expense[account]"
      assert_select "input#expense_value[name=?]", "expense[value]"
    end
  end
end
