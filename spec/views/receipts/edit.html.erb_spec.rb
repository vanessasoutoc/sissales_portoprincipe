require 'spec_helper'

describe "receipts/edit" do
  before(:each) do
    @receipt = assign(:receipt, stub_model(Receipt,
      :order => nil,
      :value => "9.99"
    ))
  end

  it "renders the edit receipt form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", receipt_path(@receipt), "post" do
      assert_select "input#receipt_order[name=?]", "receipt[order]"
      assert_select "input#receipt_value[name=?]", "receipt[value]"
    end
  end
end
