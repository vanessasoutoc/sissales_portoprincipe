require 'spec_helper'

describe "receipts/new" do
  before(:each) do
    assign(:receipt, stub_model(Receipt,
      :order => nil,
      :value => "9.99"
    ).as_new_record)
  end

  it "renders new receipt form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", receipts_path, "post" do
      assert_select "input#receipt_order[name=?]", "receipt[order]"
      assert_select "input#receipt_value[name=?]", "receipt[value]"
    end
  end
end
